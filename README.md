# Schibsted Backend Test

## How to run & test

Download the source code, open with VS17 and run it.

To run the tests, use the Test Explorer on VS17.

## API

The following methods are exposed in the API:

GET api/Users/GetUsers	
Gets all the users.

GET api/Users/GetUser/{username}	
Gets a user by username

POST api/Users/CreateUser	
Creates a new user

POST api/Users/EditUser/{username}	
Edits a user by username

DELETE api/Users/DeleteUser/{username}	
Deletes a user by username

## Users

There are 4 uses created by default:
admin
user1
user2
user3

The passwords are the same as the usernames, the admin user has the role ADMIN that can be used to create other users with the API.
Each other user has the role PAGE_X to access each page.