﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchibstedBackendTest.Entities.Contracts
{
    public interface IUser
    {
        string Username { get; set; }
        List<string> Roles { get; set; }
        string Password { get; set; }
    }
}
