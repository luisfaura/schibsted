﻿using SchibstedBackendTest.Entities.Contracts;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SchibstedBackendTest.Entities.Users
{
    [DataContract]
    public class User : IUser, IEquatable<User>
    {

        #region Constructors

        public User()
        {
        }

        public User(string username)
        {
            this.Username = username;
        }

        public User(string username, string password) : this(username)
        {
            this.Password = password;
        }

        public User(string username, List<string> roles) : this(username)
        {
            this.Roles = roles;
        }

        public User(string username, string password, List<string> roles) : this(username, password)
        {
            this.Roles = roles;
        }

        #endregion

        #region Public Properties

        [DataMember(EmitDefaultValue = false)]
        public string Username { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> Roles { get; set; }

        #endregion

        #region Overrides

        public static bool operator ==(User obj1, User obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (obj1 is null || obj2 is null)
            {
                return false;
            }

            return obj1.Username.ToLowerInvariant().Equals(obj2.Username.ToLowerInvariant());
        }

        public static bool operator !=(User obj1, User obj2)
        {
            return !(obj1 == obj2);
        }

        public bool Equals(User other)
        {
            if (other is null)
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.Username.ToLowerInvariant().Equals(other.Username.ToLowerInvariant());
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return obj.GetType() == GetType() && Equals((User)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return this.Username.ToLowerInvariant().GetHashCode();
            }
        }

        #endregion

    }
}
