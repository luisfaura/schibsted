﻿using SchibstedBackendTest.Business.Contracts;
using SchibstedBackendTest.Data.Contracts;
using SchibstedBackendTest.Entities.Contracts;
using SchibstedBackendTest.Entities.Users;
using System.Collections.Generic;
using System.Linq;

namespace SchibstedBackendTest.Business.Users
{
    public class UserService : IUserService
    {
        #region Private properties

        private readonly IUserDataService UserDataService = null;

        #endregion

        #region Constructor

        public UserService(IUserDataService userDataService)
        {
            this.UserDataService = userDataService;
        }

        #endregion

        #region Private methods

        private void SaveUser(User user)
        {
            this.UserDataService.Save(user);
        }

        public IUser GetFullUser(string username)
        {
            return this.UserDataService.GetUser(username);
        }

        #endregion

        #region Public methods

        public IUser AuthorizeUser(string username, string password)
        {
            IUser user = this.GetFullUser(username);

            return user != null && user.Password == password ? user : null;
        }

        public IEnumerable<IUser> GetUsers()
        {
            return this.UserDataService.GetUsers().Select(u => new User(u.Username, u.Roles));
        }

        public IUser GetUser(string username)
        {
            IUser user = this.GetFullUser(username);

            if (user != null)
            {
                return new User(user.Username, user.Roles);
            }

            return null;
        }

        public void CreateUser(User user)
        {
            this.SaveUser(user);
        }

        public void EditUser(string username, User user)
        {
            user.Username = username;
            this.SaveUser(user);
        }

        public void DeleteUser(string username)
        {
            this.UserDataService.Delete(new User(username));
        }

        #endregion
    }
}
