﻿using SchibstedBackendTest.Entities.Contracts;
using SchibstedBackendTest.Entities.Users;
using System.Collections.Generic;

namespace SchibstedBackendTest.Business.Contracts
{
    public interface IUserService
    {
        IEnumerable<IUser> GetUsers();
        IUser GetUser(string username);
        void CreateUser(User user);
        void EditUser(string username, User user);
        void DeleteUser(string username);
        IUser AuthorizeUser(string username, string password);
    }
}
