﻿using SchibstedBackendTest.Entities.Contracts;
using SchibstedBackendTest.Entities.Users;
using System.Collections.Generic;

namespace SchibstedBackendTest.Data.Contracts
{
    public interface IUserDataService
    {
        IEnumerable<IUser> GetUsers();
        IUser GetUser(string username);
        void Save(User user);
        void Delete(User username);
    }
}
