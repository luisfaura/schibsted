﻿using Newtonsoft.Json;
using SchibstedBackendTest.Data.Contracts;
using SchibstedBackendTest.Entities.Contracts;
using SchibstedBackendTest.Entities.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SchibstedBackendTest.Data.Users
{
    public class UserDataService : IUserDataService
    {
        #region Privare Properties

        private static readonly string CACHE_FILE = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"usersCache.json");

        private List<User> UsersCache { get; set; }

        #endregion

        #region Constructors

        public UserDataService()
        {
            this.LoadUsersCache();
        }

        #endregion

        #region Private Methods

        private void LoadUsersCache()
        {
            if (File.Exists(CACHE_FILE))
            {
                this.UsersCache = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(CACHE_FILE));
            }
            else
            {
                this.UsersCache = new List<User> { new User("admin", "admin", new List<string> { "ADMIN" }) };
                this.SaveUsersCache();
            }
        }

        private void SaveUsersCache()
        {
            File.WriteAllText(CACHE_FILE, JsonConvert.SerializeObject(this.UsersCache));
        }

        #endregion

        #region Public Methods

        public IEnumerable<IUser> GetUsers()
        {
            return this.UsersCache;
        }

        public IUser GetUser(string username)
        {
            User findUser = new User(username);
            return this.UsersCache.FirstOrDefault(u => u == findUser);
        }

        public void Save(User user)
        {
            IUser oldUser = this.UsersCache.FirstOrDefault(u => u == user);
            if (oldUser != null)
            {
                oldUser.Roles = user.Roles ?? oldUser.Roles;
                oldUser.Password = user.Password ?? oldUser.Password;
            }
            else
            {
                this.UsersCache.Add(user);
            }
            this.SaveUsersCache();
        }

        public void Delete(User user)
        {
            this.UsersCache.Remove(user);
            this.SaveUsersCache();
        }

        #endregion

    }
}
