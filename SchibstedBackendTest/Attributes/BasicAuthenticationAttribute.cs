﻿using SchibstedBackendTest.Business.Contracts;
using SchibstedBackendTest.Entities.Contracts;
using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace SchibstedBackendTest.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class BasicAuthenticationAttribute : AuthorizationFilterAttribute
    {

        private readonly IUserService usersService = null;

        public BasicAuthenticationAttribute()
        {
            this.usersService = DependencyResolver.Current.GetService<IUserService>();
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            (string username, string password) = ParseAuthorizationHeader(actionContext);
            if (username == null || password == null)
            {
                Challenge(actionContext);
                return;
            }

            IUser user = OnAuthorizeUser(username, password, actionContext);

            if (user == null)
            {
                Challenge(actionContext);
                return;
            }

            var principal = new GenericPrincipal(new GenericIdentity(username, "Basic"), user.Roles?.ToArray());

            Thread.CurrentPrincipal = principal;

            // inside of ASP.NET this is required
            if (HttpContext.Current != null)
                HttpContext.Current.User = principal;

            base.OnAuthorization(actionContext);
        }

        protected virtual IUser OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            return this.usersService.AuthorizeUser(username, password);
        }

        protected virtual (string, string) ParseAuthorizationHeader(HttpActionContext actionContext)
        {
            string authHeader = null;
            var auth = actionContext.Request.Headers.Authorization;
            if (auth != null && auth.Scheme == "Basic")
                authHeader = auth.Parameter;

            if (string.IsNullOrEmpty(authHeader))
                return (null, null);

            authHeader = Encoding.Default.GetString(Convert.FromBase64String(authHeader));

            var tokens = authHeader.Split(new char[] { ':' }, 2);
            if (tokens.Length < 2)
                return (null, null);

            return (tokens[0], tokens[1]);
        }


        void Challenge(HttpActionContext actionContext)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }

    }
}