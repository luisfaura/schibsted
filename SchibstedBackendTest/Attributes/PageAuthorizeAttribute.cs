﻿using SchibstedBackendTest.Business.Contracts;
using SchibstedBackendTest.Business.Users;
using SchibstedBackendTest.Data.Users;
using SchibstedBackendTest.Entities.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace SchibstedBackendTest.Attributes
{
    public class PageAuthorizeAttribute : AuthorizeAttribute
    {

        protected virtual IIdentity CurrentUser => HttpContext.Current?.User?.Identity;

        private readonly IUserService usersService = null;

        public PageAuthorizeAttribute()
        {
            this.usersService = DependencyResolver.Current.GetService<IUserService>();
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (CurrentUser == null || !CurrentUser.IsAuthenticated)
            {
                string username = ParseAuthorizationCookie(httpContext);
                if (username == null)
                {
                    return false;
                }

                IUser user = this.usersService.GetUser(username);

                if (user == null)
                {
                    return false;
                }

                var principal = new GenericPrincipal(new GenericIdentity(username, "Basic"), user.Roles?.ToArray());

                Thread.CurrentPrincipal = principal;

                if (HttpContext.Current != null)
                    HttpContext.Current.User = principal;
            }
            return base.AuthorizeCore(httpContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!CurrentUser.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult
                    (new RouteValueDictionary
                    (new
                    {
                        controller = "Login",
                        action = "Index",
                        returnUrl = filterContext.HttpContext.Request.Path
                    }
                    ));
            }
            else
            {
                filterContext.Result = new HttpStatusCodeResult(403, "Forbidden");
            }
        }

        protected string ParseAuthorizationCookie(HttpContextBase httpContext)
        {
            var auth = httpContext.Request.Cookies["AuthCookie"];
            if (auth != null && auth.Value != null)
            {
                var e = FormsAuthentication.Decrypt(auth.Value);
                if (!e.Expired && e.Name != null)
                {
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                    (
                        e.Version, e.Name, e.IssueDate, DateTime.Now.AddMinutes(5), false, e.UserData
                    );
                    string enTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie("AuthCookie", enTicket);
                    httpContext.Request.Cookies.Add(faCookie);
                    return e.Name;
                }
            }
            return null;
        }

    }
}