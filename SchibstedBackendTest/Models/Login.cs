﻿using System.ComponentModel.DataAnnotations;

namespace SchibstedBackendTest.Models
{
    public class LoginView
    {
        [Required]
        [Display(Name = "User Name")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}