﻿using SchibstedBackendTest.Attributes;
using SchibstedBackendTest.Business.Contracts;
using SchibstedBackendTest.Entities.Contracts;
using SchibstedBackendTest.Entities.Users;
using System.Collections.Generic;
using System.Web.Http;

namespace SchibstedBackendTest.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {

        private readonly IUserService service = null;

        public UsersController(IUserService userService)
        {
            this.service = userService;
        }

        /// <summary>
        /// Gets all the users.
        /// </summary>
        [Route("GetUsers")]
        public IEnumerable<IUser> GetUsers()
        {
            return this.service.GetUsers();
        }

        /// <summary>
        /// Gets a user by username
        /// </summary>
        /// <param name="username">The username from the user to fetch</param>
        [Route("GetUser/{username}")]
        public IUser GetUser(string username)
        {
            return this.service.GetUser(username);
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="user">The new user data</param>
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        [Route("CreateUser")]
        public void CreateUser([FromBody]User user)
        {
            this.service.CreateUser(user);
        }

        /// <summary>
        /// Edits a user by username
        /// </summary>
        /// <param name="username">The username from the user to edit</param>
        /// <param name="user">The new data</param>
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        [Route("EditUser/{username}")]
        public void EditUser(string username, [FromBody]User user)
        {
            this.service.EditUser(username, user);
        }

        /// <summary>
        /// Deletes a user by username
        /// </summary>
        /// <param name="username">The username from the user to delete</param>
        [HttpDelete]
        [Authorize(Roles = "ADMIN")]
        [Route("DeleteUser/{username}")]
        public void DeleteUser(string username)
        {
            this.service.DeleteUser(username);
        }
    }
}