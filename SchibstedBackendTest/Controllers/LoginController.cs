﻿using Newtonsoft.Json;
using SchibstedBackendTest.Business.Contracts;
using SchibstedBackendTest.Entities.Contracts;
using SchibstedBackendTest.Entities.Users;
using SchibstedBackendTest.Models;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SchibstedBackendTest.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserService service = null;

        public LoginController(IUserService userService)
        {
            this.service = userService;
        }

        [HttpGet]
        public ActionResult Index(string ReturnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                return LogOut();
            }
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginView loginView, string ReturnUrl = "")
        {
            if (ModelState.IsValid)
            {
                IUser user = this.service.AuthorizeUser(loginView.Username, loginView.Password);
                if (user != null)
                {
                    string userData = JsonConvert.SerializeObject(new User(user.Username, user.Roles));
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                        (
                        1, loginView.Username, DateTime.Now, DateTime.Now.AddMinutes(5), false, userData
                        );

                    string enTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie("AuthCookie", enTicket);
                    Response.Cookies.Add(faCookie);

                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            ModelState.AddModelError("Password", "Something went wrong. The Username or Password is invalid");
            return View(loginView);
        }

        public ActionResult LogOut()
        {
            HttpCookie cookie = new HttpCookie("AuthCookie", "")
            {
                Expires = DateTime.Now.AddYears(-1)
            };
            Response.Cookies.Add(cookie);

            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login", null);
        }
    }
}