﻿using SchibstedBackendTest.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchibstedBackendTest.Controllers
{
    [PageAuthorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [PageAuthorize(Roles = "PAGE_1")]
        public ActionResult Page1()
        {
            ViewBag.Title = "Page 1";

            return View();
        }

        [PageAuthorize(Roles = "PAGE_2")]
        public ActionResult Page2()
        {
            ViewBag.Title = "Page 2";

            return View();
        }

        [PageAuthorize(Roles = "PAGE_3")]
        public ActionResult Page3()
        {
            ViewBag.Title = "Page 3";

            return View();
        }
    }
}
