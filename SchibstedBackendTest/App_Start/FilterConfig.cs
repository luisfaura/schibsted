﻿using SchibstedBackendTest.Attributes;
using System.Web;
using System.Web.Mvc;

namespace SchibstedBackendTest
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
