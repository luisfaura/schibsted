﻿using SchibstedBackendTest.Business.Contracts;
using SchibstedBackendTest.Business.Users;
using SchibstedBackendTest.Controllers;
using SchibstedBackendTest.Data.Contracts;
using SchibstedBackendTest.Data.Users;
using SchibstedBackendTest.Entities.Contracts;
using SchibstedBackendTest.Entities.Users;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Lifetime;

namespace SchibstedBackendTest
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();
            container.RegisterType<IUserService, UserService>(new SingletonLifetimeManager());
            container.RegisterType<IUserDataService, UserDataService>(new SingletonLifetimeManager());
            container.RegisterType<IUser, User>();
            container.RegisterType<IController, LoginController>("Login");
            var resolver = new UnityResolver(container);
            DependencyResolver.SetResolver(resolver);
            config.DependencyResolver = resolver;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
