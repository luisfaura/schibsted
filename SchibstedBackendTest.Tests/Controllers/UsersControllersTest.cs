﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchibstedBackendTest.Business.Users;
using SchibstedBackendTest.Controllers;
using SchibstedBackendTest.Data.Users;
using SchibstedBackendTest.Entities.Contracts;
using SchibstedBackendTest.Entities.Users;
using System.Collections.Generic;
using System.Linq;

namespace SchibstedBackendTest.Tests.Controllers
{
    [TestClass]
    public class UsersControllersTest
    {

        [TestMethod]
        public void GetUsersTest()
        {
            UsersController usersCtrl = new UsersController(new UserService(new UserDataService()));

            IEnumerable<IUser> users = usersCtrl.GetUsers();

            Assert.IsNotNull(users);
            Assert.IsTrue(users.ToList().Count > 0);
        }

        [TestMethod]
        public void GetUserTest()
        {
            UsersController usersCtrl = new UsersController(new UserService(new UserDataService()));

            IUser user = usersCtrl.GetUser("admin");

            Assert.IsNotNull(user);
            Assert.IsTrue(user.Username == "admin");
        }

        [TestMethod]
        public void CreateUserTest()
        {
            UsersController usersCtrl = new UsersController(new UserService(new UserDataService()));

            usersCtrl.CreateUser(new User("test", "test", new List<string> { "TEST" }));

            IUser user = usersCtrl.GetUser("test");

            Assert.IsNotNull(user);
            Assert.IsTrue(user.Roles.Count == 1);
            Assert.IsTrue(user.Roles[0] == "TEST");

            usersCtrl.DeleteUser(user.Username);
        }

        [TestMethod]
        public void EditUserTest()
        {
            UsersController usersCtrl = new UsersController(new UserService(new UserDataService()));

            usersCtrl.CreateUser(new User("test", "test", new List<string> { "TEST" }));

            IUser user = usersCtrl.GetUser("test");

            Assert.IsNotNull(user);
            Assert.IsTrue(user.Roles.Count == 1);
            Assert.IsTrue(user.Roles[0] == "TEST");

            usersCtrl.EditUser("test", new User(null, "test2", new List<string> { "TEST2" }));

            user = usersCtrl.GetUser("test");

            Assert.IsNotNull(user);
            Assert.IsTrue(user.Roles.Count == 1);
            Assert.IsTrue(user.Roles[0] == "TEST2");

            usersCtrl.DeleteUser(user.Username);
        }

        [TestMethod]
        public void DeleteUserTest()
        {
            UsersController usersCtrl = new UsersController(new UserService(new UserDataService()));

            usersCtrl.CreateUser(new User("test", "test", new List<string> { "TEST" }));

            usersCtrl.DeleteUser("test");

            IUser user = usersCtrl.GetUser("test");

            Assert.IsNull(user);
        }
    }
}
